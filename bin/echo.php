<?php

$root = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR;
$file = implode(DIRECTORY_SEPARATOR, array('test', 'fool', 'executor', 'framework', 'executable', 'EchoProgram'));
require_once($root . $file . '.php');
use \fool\test\executor\framework\executable\EchoProgram;

$echo = new EchoProgram($_SERVER['argv']);
$echo->run();
