<?php

$arguments = $_SERVER['argv'];
if (count($arguments) < 2) {
    exit(1);
}
$time = floatval($arguments[1]);

echo "Sleeping for $time", PHP_EOL;
sleep($time);
echo "Done sleeping for $time",PHP_EOL;

