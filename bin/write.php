<?php

$data = stream_get_contents(STDIN);
$file = 'write.txt';
$bytesWritten = file_put_contents($file, $data);

$error = $bytesWritten === false;
$writeSuccess = $bytesWritten === strlen($data);

if ($error) {
    exit(1);
} else if (!$writeSuccess) {
    exit (2);
}
// else: success, exit 0
