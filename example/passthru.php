<?php
$ds = DIRECTORY_SEPARATOR;
$root = dirname(dirname(__FILE__)) . $ds;
$bootstrapFile = "{$root}test{$ds}fool{$ds}executor{$ds}framework{$ds}bootstrap.php";
require $bootstrapFile;
use fool\executor\Passthru;

/**
 * Using passthru() to execute:
 *
 * php bin/echo.php hello world
 * > hello world
 */
$passthru = new Passthru();
$passthru->setProgram('php');
$passthru->addArgument("{$root}bin{$ds}echo.php");
$passthru->addArgument('hello');
$passthru->addArgument('world');

$passthru->execute();
