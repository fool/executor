<?php
$ds = DIRECTORY_SEPARATOR;
$root = dirname(dirname(__FILE__)) . $ds;
$bootstrapFile = "{$root}test{$ds}fool{$ds}executor{$ds}framework{$ds}bootstrap.php";
require $bootstrapFile;
use fool\executor\Popen;

/**
 * Using popen() to execute:
 *
 * php bin/echo.php hello world
 * > hello world
 */
$passthru = new Popen('', array(), 'w');
$passthru->setProgram('php');
$passthru->addArgument("{$root}bin{$ds}echo.php");
$passthru->addArgument('hello');
$passthru->addArgument('world');

$resource = $passthru->execute();
$output = stream_get_contents($resource);
pclose($resource);
echo $output;
