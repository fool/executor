<?php
$ds = DIRECTORY_SEPARATOR;
$root = dirname(dirname(__FILE__)) . $ds;
$bootstrapFile = "{$root}test{$ds}fool{$ds}executor{$ds}framework{$ds}bootstrap.php";
require $bootstrapFile;
use fool\executor\ProcOpen;


/**
 * Using proc_open() to execute:
 *
 * php bin/echo.php hello world
 * > hello world
 */
$procOpen = new ProcOpen();
$procOpen->setProgram('php');
$procOpen->addArgument("{$root}bin{$ds}echo.php");
$procOpen->addArgument('hello');
$procOpen->addArgument('world');
$procOpen->execute();
echo stream_get_contents($procOpen->getStandardOut());
$procOpen->close();
