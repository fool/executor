<?php

namespace fool\executor;

/**
 * Executor is a wrapper for the PHP command execution functions. The interface
 * prevents all command injection.
 */
abstract class Executor
{
    const VERSION = "1.3.0";

    /**
     * A list of arguments to pass to the program
     *
     * @var array
     */
    protected $arguments = array();

    /**
     * The name of the program to execute
     *
     * @var string
     */
    protected $program;

    /**
     * After the program has been executed, this will contain the last command given to the system.
     *
     * @var string
     */
    protected $lastExecutedCommand;

    /**
     * @param string $program    Optionally set the program name
     * @param array  $arguments  Optionally set the arguments
     */
    public function __construct($program = '', array $arguments = array())
    {
        if ($program) {
            $this->setProgram($program);
        }

        if ($arguments) {
            $this->addArguments($arguments);
        }
    }

    /**
     * Add an argument to the program. This adds the argument to the end of the lisst.
     *
     * @param string $value
     */
    public function addArgument($value)
    {
        $this->arguments[] = $value;
    }

    /**
     * Add a list of arguments.
     *
     * @param array $arguments
     */
    public function addArguments(array $arguments)
    {
        foreach ($arguments as $argument) {
            $this->addArgument($argument);
        }
    }

    /**
     * Replace all existing arguments with a new list.
     *
     * @param array $arguments
     */
    public function setArguments(array $arguments)
    {
        $this->arguments = $arguments;
    }

    /**
     * @param string $name
     */
    public function setProgram($name)
    {
        $this->program = $name;
    }

    /**
     * creates the escaped command string
     * @return string
     */
    protected function buildCommand()
    {
        /* do not attempt to create invalid commands */
        if (!$this->program) {
            $command = '';
        } else {
            $command = escapeshellarg($this->program) . ' ';
            foreach ($this->arguments as $arg) {
                $command .= escapeshellarg($arg) . ' ';
            }
        }
        return $command;
    }

    /**
     * Executes the process.
     *
     * This wraps the calls in the subclasses to ensure buildCommand() is used to generate
     * the command safely. This also guarantees $this->lastExecutedCommand is set properly.
     *
     * @return mixed
     */
    public function execute()
    {
        $command = $this->buildCommand();
        $this->lastExecutedCommand = $command;
        return $this->executeCommand($command);
    }

    /**
     * Run the command. The different versions available try to mimic the php original function's behavior.
     *
     * @param  string $command
     * @return mixed
     */
    abstract protected function executeCommand($command);


    /**
     * Returns what command would be executed right now if execute() were called
     *
     * @return string
     */
    public function getCommand()
    {
        return $this->buildCommand();
    }

    /**
     * Returns the last executed command as a string
     *
     * @return string
     */
    public function getLastExecutedCommand()
    {
        return $this->lastExecutedCommand;
    }

    /**
     * @return string[]
     */
    public function getArguments()
    {
        return $this->arguments;
    }
}
