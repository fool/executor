<?php


namespace fool\executor;
use \Exception;

/**
 * Thrown when you try to add a pipe to a file using Proc Open
 * but giving an invalid mode.
 */
class InvalidFileModeException extends Exception
{
    public function __construct($mode, array $possibleModes)
    {
        $message = "$mode is not one of the valid modes: (" . implode(', ', $possibleModes) . ')';
        parent::__construct($message);
    }
} 