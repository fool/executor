<?php

namespace fool\executor;
use \Exception;

/**
 * Thrown when you try to run execute() with an invalid mode or set this mode to something invalid
 */
class InvalidPopenModeException extends Exception
{
    /**
     * @param string $mode
     */
    public function __construct($mode)
    {
        parent::__construct("Invalid popen() mode: $mode");
    }
}
