<?php


namespace fool\executor;


/**
 * Wrapper for php's popen()
 */
class Popen extends Executor
{
    /**
     * A list of all the valid popen modes
     * @var string[]
     *
     */
    private static $validModes = array('r', 'r+', 'w', 'w+', 'a', 'a+', 'x', 'x+', 'c', 'c+');

    /**
     * The mode to give to popen
     *
     * @var string
     */
    protected $mode;

    /**
     * Only available after popen() has been called.
     * If execute is called multiple times this contains the last resource returned by popen().
     *
     * @var resource
     */
    protected $resource;

    /**
     * @param string $program
     * @param array  $arguments
     * @param string $mode
     */
    public function __construct($program = '', array $arguments = array(), $mode = '')
    {
        parent::__construct($program, $arguments);
        if ($mode) {
            $this->mode = $mode;
        }
    }

    /**
     * @param  string $command
     * @return resource
     * @throws InvalidPopenModeException
     */
    protected function executeCommand($command)
    {
        if (!$this->isValidMode($this->mode)) {
            throw new InvalidPopenModeException($this->mode);
        }
        $resource = popen($command, $this->mode);
        $this->resource = $resource;
        return $resource;
    }

    /**
     * can't use in_array() because it only does weak match
     *
     * @param  string $mode
     * @return bool
     */
    protected function isValidMode($mode)
    {
        foreach (self::$validModes as $validMode) {
            if ($validMode === $mode) {
                return true;
            }
        }
        return false;
    }

    /**
     * Only available after execute() has been called
     *
     * @return resource
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param   string $mode
     * @@throws InvalidPopenModeException
     */
    public function setMode($mode)
    {
        if (!$this->isValidMode($mode)) {
            throw new InvalidPopenModeException($mode);
        }
        $this->mode = $mode;
    }
}
