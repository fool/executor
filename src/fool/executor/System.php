<?php


namespace fool\executor;


class System extends Executor
{
    use ExitStatus;

    /**
     * @param  string $command
     * @return string The last line of output or boolean false on command failure
     */
    protected function executeCommand($command)
    {
        $exitStatus = 0;
        $result = system($command, $exitStatus);
        $this->exitStatus = $exitStatus;
        return $result;
    }
}
