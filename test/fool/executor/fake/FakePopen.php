<?php


namespace fool\test\executor\fake;


use fool\executor\Popen;

/**
 * @package fool\test\executor\fake
 */
class FakePopen extends Popen
{
    /**
     * @param  string $mode
     * @return bool
     */
    public function testIsValidMode($mode)
    {
        return $this->isValidMode($mode);
    }
} 