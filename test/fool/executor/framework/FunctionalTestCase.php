<?php


namespace fool\test\executor\framework;

class FunctionalTestCase extends ExecutorTestCase
{
    /**
     * @var string
     */
    private static $root;

    /**
     * The bin directory
     *
     * @var string
     */
    protected $bin;

    /**
     * @var string
     */
    protected $echo;

    /**
     * @var string
     */
    protected $capitalize;

    /**
     * @var string
     */
    protected $pwd;

    /**
     * @var string
     */
    protected $env;

    /**
     * @var string
     */
    protected $slow;

    /**
     * @var string
     */
    protected $printer;

    /**
     * @var string
     */
    protected $silent;

    /**
     * @var string
     */
    protected $pid;

    /**
     * @var string
     */
    protected $write;

    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->bin = $this->getBinRoot();
        $this->echo = $this->bin . 'echo.php';
        $this->capitalize = $this->bin . 'cap.php';
        $this->slow = $this->bin . 'slow.php';
        $this->pwd = $this->bin . 'pwd.php';
        $this->env = $this->bin . 'env.php';
        $this->printer = $this->bin . 'printer.php';
        $this->silent = $this->bin . 'silent.php';
        $this->pid = $this->bin . 'pid.php';
        $this->write = $this->bin . 'write.php';
    }

    /**
     * @return string
     */
    protected  function getRoot()
    {
        if (self::$root) {
            return self::$root;
        }

        self::$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
        return self::$root;
    }

    /**
     * @return string
     */
    protected  function getBinRoot()
    {
        return $this->getRoot() . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR;
    }
}
