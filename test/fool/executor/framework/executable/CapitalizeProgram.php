<?php


namespace fool\test\executor\framework\executable;

/**
 * Capitalizes stuff
 */
class CapitalizeProgram extends EchoProgram
{
    /**
     * @param string $message
     */
    protected function echoMessage($message)
    {
        echo strtoupper($message);
    }
}
