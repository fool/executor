<?php


namespace fool\test\executor\functional;
use \fool\executor\ShellExec;
use fool\test\executor\framework\FunctionalTestCase;

class ShellExecTest extends FunctionalTestCase
{
    public function testEchoHelloWorld()
    {
        $shellExec = new ShellExec();

        $shellExec->setProgram('php');
        $shellExec->addArgument($this->echo);
        $shellExec->addArgument('hello');
        $shellExec->addArgument('world');

        $result = $shellExec->execute();
        $expectedResult = 'hello world' . PHP_EOL;

        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedResult, $shellExec->getOutput());
    }
}
