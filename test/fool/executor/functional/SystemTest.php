<?php


namespace fool\test\executor\functional;


use fool\executor\System;
use fool\test\executor\framework\FunctionalTestCase;

class SystemTest extends FunctionalTestCase
{
    public function testEchoHelloWorld()
    {
        $system = new System();

        $system->setProgram('php');
        $system->addArgument($this->echo);
        $system->addArgument('hello');
        $system->addArgument('world');

        $result = $system->execute();
        $expectedResult = 'hello world';

        $this->assertEquals(0, $system->getExitStatus());
        $this->assertEquals($expectedResult, $result);
    }
} 