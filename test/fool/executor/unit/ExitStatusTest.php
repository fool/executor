<?php


namespace fool\test\executor\unit;
use \fool\test\executor\fake\FakeExitStatus;
use fool\test\executor\framework\UnitTestCase;

class ExitStatusTest extends UnitTestCase
{
    public function testGetExitStatusReturnsExitStatus()
    {
        $exitStatus = 5;
        $exit = new FakeExitStatus($exitStatus);
        $this->assertEquals($exitStatus, $exit->getExitStatus(), "getExitStatus() was incorrect");
    }

    public function dataProviderForTestWasSucessful()
    {
        return array(
            array(0,  true),
            array(1,  false),
            array(-1, false),
            array(2,  false),
            array(3,  false),
            array(10, false),
        );
    }

    /**
     * @dataProvider dataProviderForTestWasSucessful
     */
    public function testWasSucessful($exitCode, $expectedReturnValue)
    {
        $exit = new FakeExitStatus($exitCode);
        $this->assertEquals($expectedReturnValue, $exit->wasSuccessful(), "Was successful gave incorrect value");
    }
} 